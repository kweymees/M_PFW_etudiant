"use strict";

const riders = [
    { name: "to to", img: "img1"},
    { name: "to to", img: "img2"},
    { name: "tata", img: "img3"}
];

console.log("\nriders\n", riders);

function p(acc, rider) {
    const name = rider.name;
    if (acc[name] === undefined)
        acc[name] = [rider.img];
    else
        acc[name].push(rider.img);
    return acc;
};

const groups = riders.reduce(p, {});

console.log("\ngroups\n", groups);

Object.keys(groups).map(console.log);

console.log("\nentries\n", Object.entries(groups));

