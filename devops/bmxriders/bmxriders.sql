CREATE TABLE bmxrider (
  id SERIAL PRIMARY KEY, 
  name TEXT,
  image TEXT 
);
INSERT INTO bmxrider (name, image) VALUES('Andy Buckworth', 'andy-buckworth.jpg');
INSERT INTO bmxrider (name, image) VALUES('Brandon Loupos', 'brandon-loupos-1.jpg');
INSERT INTO bmxrider (name, image) VALUES('Brandon Loupos', 'brandon-loupos-2.jpg');
INSERT INTO bmxrider (name, image) VALUES('Dave Mirra', 'dave-mirra.jpg');
INSERT INTO bmxrider (name, image) VALUES('Harry Main', 'harry-main.jpg');
INSERT INTO bmxrider (name, image) VALUES('Logan Martin', 'logan-martin-1.jpg');
INSERT INTO bmxrider (name, image) VALUES('Logan Martin', 'logan-martin-2.png');
INSERT INTO bmxrider (name, image) VALUES('Logan Martin', 'logan-martin-3.jpg');
INSERT INTO bmxrider (name, image) VALUES('Mark Webb', 'mark-webb-1.jpg');
INSERT INTO bmxrider (name, image) VALUES('Mark Webb', 'mark-webb-2.jpg');
INSERT INTO bmxrider (name, image) VALUES('Matt Hoffman', 'matt-hoffman.jpg');
INSERT INTO bmxrider (name, image) VALUES('Pat Casey', 'pat-casey-1.jpg');
INSERT INTO bmxrider (name, image) VALUES('Pat Casey', 'pat-casey-2.jpg');

