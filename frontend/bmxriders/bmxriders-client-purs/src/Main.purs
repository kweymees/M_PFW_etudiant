module Main where

import Control.Monad.Aff (Aff)
import Control.Monad.Eff (Eff)
-- import Data.Argonaut ((.?), class DecodeJson, decodeJson, Json)
-- import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
-- import Data.Traversable (traverse)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
-- import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Network.HTTP.Affjax as AX
import Prelude

main :: Eff (HA.HalogenEffects (ajax :: AX.AJAX)) Unit
main = HA.runHalogenAff do
    body <- HA.awaitBody
    io <- runUI ui unit body
    io.query $ H.action $ SetModelText ""

ui :: forall eff. H.Component HH.HTML Query Unit Void (Aff (ajax :: AX.AJAX | eff))
ui = H.component
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }

type State = { modelText :: String }

data Query a = SetModelText String a

initialState :: State
initialState = { modelText: "" }

render :: State -> H.ComponentHTML Query
render st =
    HH.span []
        [ HH.h1 [] [ HH.text "Test input" ]
        , HH.p [] [ HH.input [ HE.onValueInput (HE.input SetModelText) ] ]
        , HH.p [] [ HH.text st.modelText ]
        ]

eval :: forall eff. Query ~> H.ComponentDSL State Query Void (Aff (ajax :: AX.AJAX | eff))
eval (SetModelText str next) = do
    H.modify (_ { modelText = str })
    pure next

