{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

-- import Data.Aeson (FromJSON)
import Data.Default (def)
-- import Data.Maybe (fromJust)
-- import Data.Monoid ((<>))
-- import Data.Text (Text)
-- import GHC.Generics (Generic)
import Prelude
import Reflex (holdDyn)
import Reflex.Dom 
-- import Reflex.Dom.Xhr (decodeXhrResponse, performRequestAsync, XhrRequest(..))

main :: IO ()
main = mainWidget ui

ui :: MonadWidget t m => m ()
ui = do
    el "h1" $ text "Bmx riders (reflex)"
    myInput <- el "p" $ textInput def
    dynInput <- holdDyn "" $ _textInput_input myInput
    el "p" $ dynText dynInput
    return ()

